// make sure there is a convenience button for the new syntax
if (typeof window.toolbar !== 'undefined') {
    toolbar[toolbar.length] = {
        type: "format",
        title: "insert a Magic: The Gathering card reference",
        icon: "../../plugins/mtg/images/mtg.png",
        key: "m",
        open: "{{mtg:",
        sample: "cardname",
        close: "}}"
    };
}